(async () => {

  class compoentTemplate extends HTMLElement {
    constructor() {  
        super();
        // define properties
        this._createProp("country", true, 2);
        this._createProp("myobject", false); //objects & arrays do no reflect or will be stringyfied
        // create shadowdoom
        this._root = this.attachShadow({mode: 'closed'});
        this._template = compoentTemplate.createTemplate();
        // Init sample variable "country"
        this.render();

        // Event handling
        this.addEventListener("click", e => {
          console.log("click!")
          //fire a custom event
          this.fireMyEvent();
        });
    }

    // Getter to let component know what attributes
    // to watch for mutation in this example: country
    static get observedAttributes() {
      return ['country']; 
    };

    static createTemplate(){
      function createStyle(){
        return `
        .myclass {
          color: blue;
        }`;
      };
      function createBody(){
        return ` <style>${createStyle()}</style>
        <span class="myclass">
          <em>Hello World!</em> ${ [1, 2, 3].map( num => 
            ` <strong>${num}</strong>`
          ).join(', ') }
          <br>country = <span id="countrycode">unknown</span>
          <br>by slot: <slot name="country"></slot>. .<slot name="country1"></slot>
        </span>`;
      }
      var wrapper = document.createElement("template")
      wrapper.innerHTML = createBody();
      return wrapper;
    }

    render(){
      //this._root.appendChild ( document.importNode(this._template.content, true) );
      this._root.appendChild ( this._template.content.cloneNode(true) );
    }

    _createProp(name , reflect=true, slotable=1){
        self = this;
        const times = n => Array.from(Array(n).keys()) 

        function createSlottedTag (slotIndex) {
          const indexName = (index) => {return index?index:''}   
          var htmlTagAttribute = document.createElement ( "span" );
          htmlTagAttribute.slot = name + indexName(slotIndex) 
          self.appendChild(htmlTagAttribute);
          return htmlTagAttribute;
        }

        function createPropertyStructure (name, reflect, slotable){
          if (self._autoProperties===undefined) self._autoProperties = {};
          self._autoProperties[name]={
            value: undefined,
            slotable: times(slotable).map( createSlottedTag ),
            reflect: reflect
          }
        }

        function createPropertySetterAndGetter(thisObject){
          Object.defineProperty(thisObject, name, {
            set: (value) => {
              var prop = thisObject._autoProperties[name];
              prop.value = value;
              prop.slotable.map(x => x.textContent = value);
              if (prop.reflect) 
                thisObject.setAttribute(name, value); 
            },
            get: () => {
              return thisObject._autoProperties[name].value;
            }
          });
        }

        createPropertyStructure (name, reflect, slotable);
        createPropertySetterAndGetter(this);

    }
    
    fireMyEvent(){
      var payload = {message: "my event!", country: this._countryCode};
      this.dispatchEvent(new CustomEvent('myevent', {detail: payload, bubbles: true}))
    }

    // Fires when an attribute was added, removed, or updated (only for listened in observedAttributes)
    attributeChangedCallback (attr, oldVal, newVal) {
      if (oldVal == newVal) return;
      console.log(`${attr} was changed from ${oldVal} to ${newVal}!`);
      if ("country" == attr) { 
        this.country=newVal;
      }
    }

    // Fires when an instance of the element is created
    createdCallback () {};
    // Fires when an instance was inserted into the document
    attachedCallback () {};
    // Fires when an instance was removed from the document
    detachedCallback () {};
    // Called when element is inserted in DOM
    connectedCallback() {};
    
  }
  
  customElements.define('component-template', compoentTemplate);
})();